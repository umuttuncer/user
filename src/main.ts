var apm = require('elastic-apm-node').start({

  // Override the service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: 'user',
    // Set the custom APM Server URL (default: http://localhost:8200)
  serverUrl: 'http://0.0.0.0:8200',
  
  // Set the service environment
  environment: 'development'
  })
  

import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport:Transport.TCP,
      options:{
        port:3005
      }
    }
  );
  app.listen()
}
bootstrap();
